# Chilean Arrival Times - 1982 - mid-2020

This repository contains the arrival times produced by the Centro Sismológico Nacional (CSN, Universidad de Chile, http://www.sismologia.cl) and revised in the present study.

This archive contains 995,954 P- and 922,004 S-waves arrival-times, corresponding to 118,004 events recorded by the CSN on 271 stations between 1982 and July 2020.

Contact:
- Bertrand Potin, _DGF, University of Chile_ (bertrand.potin@uchile.cl),
- Sergio Ruiz, _DGF, University of Chile_ (sruiz@uchile.cl),
- Sergio Barrientos, _CSN, University of Chile_ (sbarrien@csn.uchile.cl)

![seismicity and station coverage](./wd/map_sismo_stations.jpg){width=60%}

_Chilean seismicity as reported by the CSN. (**A**): relocated CSN catalogue, from 1982 to July 2020. Colors represent depth. Shallower earthquakes are plotted on top. (**B**): Stations used by the CSN between 1982 and 2020. Colors indicate number of arrival times per station. Some stations correspond to temporary deployments, others are integrated through colaborations with other organizations._

Arrival-times are formated by events in a text file. The corresponding station coordinates are given as a CSV archive. These arrival times correspond to the catalogue published in Potin _et al._ (2024) and distributed under the DOI https://doi.org/10.5281/zenodo.13146436

## How to cite this material

### Material doi

https://doi.org/10.5281/zenodo.13173435

### Related article

Potin, B., S. Ruiz, F. Aden-Antoniow, R. Madariaga, and S. Barrientos (2024). A Revised Chilean Seismic Catalog from 1982 to Mid-2020, _Seismol. Res. Lett._. https://doi.org/10.1785/0220240047

## Files format

### CSN_1982-2020_arrival-times.light_ins

The file correspond to a list of earthquakes, starting with a header line and followed by the corresponding arrival-times. Events are separated by an empty line.

#### Light Insight format:

##### header line:
```
YYMMDD hhmm sss.ss long.xxxxxx lat.xxxxxx dep.xxxx RMSxx RMSsx MAGxx # T
```
- `YYMMDD hhmm`: year month day hour minute: reference time for the event (i2i2i2 i2i2).
- `sss.ss`: seconds for the event origin, relative to reference time (f6.2). Might be negative or > 60.0
- `long.xxxxxx`: longitude (f11.6)
- `lat.xxxxxx`: latitude (f10.6)
- `dep.xxxx`: depth (km) (f8.4)
- `RMSxx`: location data ajustement (s) (f5.2)
- `RMSsx`: normalized location data ajustement (sigma) (f5.2)
- `MAGxx`: magnitude, either a number of the mention `NOMAG` (f5.2 or str) 
- `T`: magnitude type, one of:
     - `l`: local magnitude,
     - `c`: coda magnitude,
     - `W`: moment magnitude, Brune's approach,
     - `ww`: moment magnitude, W-phase approach,
     - `xx`: no magnitude.

##### Data line:
```
STAxx azi.x inc.x ph p s.ig sss.ss
```
- `STAxx`: station code (s5)
- `azi.x`: ray azimut [0.0 360.0] (f5.1)
- `inc.x`: ray inclinaison [0 180.0] (f5.1) (0 is up)
- `ph`: phase: "P ", "S ", "Pg", "Pn", "P1", "P2", ... (s2)
- `p`: polarity: " ", "C", "D", "U", "+", "-"
- `s.ig`: uncertainty (s) (f4.2)
- `sss.ss`: arrival time, relative to reference time (s) (f6.2). Might be negative or > 60.0

#### Example

```
191103 0328  22.02  -67.289814 -24.180293 201.3188  0.36  0.76  2.80 # l
PB19  273.5  37.6 P1   0.46  59.69
PB02  331.4  51.3 P1   0.46  80.66
GO02  237.9  57.7 P1   0.46  63.85
PB09  332.1  50.4 P1   0.46  72.56
PB05  301.3  41.6 P1   0.46  71.64
PB06  309.5  42.5 P1   0.46  66.76
PB03  322.1  47.4 P1   0.46  73.51
PB19  275.8  37.6 S1   0.68  88.59
PB06  308.2  45.6 S1   0.68 101.08
PB05  294.3  57.3 S1   0.68 109.20 # <<==
PB09  327.6  50.7 S1 D 0.68 110.44

191103 0806  11.15  -72.036989 -30.727443   6.0101  0.69  1.52  5.20 # ww
CO06   75.5 118.6 S1   0.48  24.34
CO06   76.6 120.1 P1 C 0.46  19.16
CO05   46.6 129.1 P1 D 0.12  30.29
CO02  113.8 130.3 P1 C 0.46  30.32
GO04   62.7 128.8 P1 D 0.12  32.85
CO01   66.7 129.5 P1 C 0.12  43.26
...
```

In this example, the line marked with the sign `<<==` corresponds to an arrival-time measured 109.20 s after the reference time. For this earthquake, reference time is `191103 0328`, which is November 3rd, 2019, at 3h28 (UTC). The arrival time is therfore 109.20 seconds after that date, at 3h29 and 49.20 seconds.

Note: the `#` character initiate a comment in Insight format. 

---

### CSN_1982-2020_station_list.csv

This CSV file contains the coordinates of the stations used to build the catalogue.

#### Format

```
index,station code,longitude,latitude,altitude
```
- `index`: index, between 0 and 270,
- `station code`: name of the station,
- `longitude`: longitude in degrees,
- `latitude`: latitude in degrees,
- `altitude`: altitude in metres.

#### Example

```
...
59,CO02,-71.000167,-31.203500,1149
60,CO03,-70.689167,-30.839000,990
61,CO04,-70.974667,-32.043333,2401
62,CO05,-71.238333,-29.918667,101
63,CO06,-71.635000,-30.673833,240
...
```

